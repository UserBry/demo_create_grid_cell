class GenericCellClass
{
    constructor(options)
    {
        this.options = options;
        this.options.cellClasses = this.options.cellClasses || [];
    }

    createCellElement(rowIndex, cellIndex)
    {
        this.element = document.createElement('div');
        this.element.style.width = this.options.cellWidth;
        this.element.style.height = this.options.cellHeight;
        this.element.dataset.rowIndex = rowIndex;
        this.element.dataset.cellIndex = cellIndex;
        this.addStyleClass('cell', ...this.options.cellClasses);

        return this.element;
    }

    addStyleClass(...classes)
    {
        this.element.classList.add(...classes);
    }

}

