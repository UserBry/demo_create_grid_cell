
const grid = new GenericGridClass({
    rowCount: 10,
    cellCount: 10,
    cellWidth: '20px',
    cellHeight: '20px',
    cellClasses: ['red'],
})
{ 
}

console.log(grid.createGridElement());

console.log(grid.findSpecificCell(0,9));
console.log(grid.swapStyle("row7","ocean"));

const eventListeners = [ 
    new EventDescription( 
    {
        listener: grid.element,
        type: 'click',
        callback: function(event)
        {
            console.log("`this` means", grid)
            console.log(event.target)
            console.log(event.currentTarget);
        },
        context: grid
    })
];  

grid.addEventListeners(eventListeners);

console.log(grid.findNearbyCells(3,3));