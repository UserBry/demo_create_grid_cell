class GenericGridClass
{
    constructor(options = {})
    {
        this.options = options;        
        this.array = [];
    }

    createGridElement()
    {
        this.element = document.querySelector('#grid-container');

        for(let rowIndex = 0; rowIndex < this.options.rowCount; rowIndex++)
        {

            const rowElement = this.createRowElement(rowIndex);
            this.element.appendChild(rowElement);
            this.array.push([]);

            for(let cellIndex = 0; cellIndex < this.options.cellCount; cellIndex++)
            {
                const cell = new GenericCellClass(this.options);
                {}
                rowElement.appendChild(cell.createCellElement(rowIndex,cellIndex));
                this.array[rowIndex].push(cell);

            }
        }

        return this.element;
    }

    createRowElement(rowIndex)
    {
        const element = document.createElement('div');
        const classString = 'row' + rowIndex;
        element.classList.add(classString);
        element.dataset.rowIndex = rowIndex;

        return element;
    }

    findSpecificCell(rowIndex,cellIndex)
    {
        const row = this.array[rowIndex] || null;   //If rowIndex is undefine, so out-of-bounds entry
        const cellInstance = row && row[cellIndex];

        return cellInstance || null;
        //return this.array[rowIndex][cellIndex]
    }

    findNearbyCells({rowIndex, cellIndex})
    {
        const directions = Object.entries( 
        {
            left: { row: +0, column: -1 },
            up: { row: -1, column: +0 },
            right: { row: +0, column: +1 },
            down: { row: +1, column: +0 },
            upleft: { row: -1, column: -1 },
            upright: { row: -1, column: +1 },
            downright: { row: +1, column: +1 },
            downleft: { row: +1, column: -1 },
        })

        const matches = directions.map(([direction, offsets]) => {
            const newRowIndex = rowIndex + offsets.row;
            const newCellIndex = cellIndex + offsets.cell;
            return [direction, this.findSpecificCell(newRowIndex,newCellIndex)]
        }).filter(([, cell]) => Boolean(cell))

        return Object.fromEntries(matches);
    }

    swapStyle(oldClass,newClass)
    {
        const originalClass = "." + oldClass;
        const selectedRow = document.querySelector(originalClass);
        selectedRow.classList.add(newClass);
        
        return selectedRow;
    }

    addEventListeners(eventDescriptions)
    {
        for(let {listener, type, callback} of eventDescriptions)
        {
            listener.addEventListener(type,callback);
        }
    }
}